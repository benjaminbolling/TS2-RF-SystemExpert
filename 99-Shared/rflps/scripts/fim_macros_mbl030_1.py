from org.csstudio.opibuilder.scriptUtil import PVUtil

# This script will populate several macros that will be used to mount the PV names inside the GUIs
# In this first version, the content of the macros are hardcoded inside this script. 
# The next step is to read the values from a file under version control;

# Section and subsection are fixed per script 
prefix_ = "MBL-030RFC:"
sec_ = "MBL"
subsec_ = "030RFC"

# Adding the macros on the widget that will consume this script
widget.getPropertyValue("macros").add("P", prefix_)
widget.getPropertyValue("macros").add("SEC", sec_)
widget.getPropertyValue("macros").add("SUB", subsec_)

##################################################################################################
#	Hardcoded macro data
##################################################################################################

# This macro refers to the IOC main PVs:
ioc_ = "RFS-FIM-101:"
ioc2_ = "Ctrl-IOC-104:"
# This macro refers to the IOCStats Modules:
iocstats_ = prefix_ + ioc2_

# Analog input macros
ai00_ = "RFS-Mod-110:Vol-"
ai01_ = "RFS-Mod-110:Cur-"
ai02_ = "RFS-SolPS-110:Cur-"
ai03_ = "RFS-SolPS-120:Cur-"
ai04_ = "RFS-FIM-101:AI5-"
ai05_ = "RFS-EPR-110:Cur-"
ai06_ = "RFS-FIM-101:AI7-"
ai07_ = "RFS-FIM-101:AI8-"
ai08_ = "RFS-FIM-101:AI9-"
ai09_ = "RFS-FIM-101:AI10-"

ai10_ = "RFS-PAmp-110:PwrFwd-"
ai11_ = "RFS-Kly-110:PwrFwd-"
ai12_ = "RFS-Load-120:PwrFwd-"
ai13_ = "RFS-Kly-110:PwrRfl-"
ai14_ = "RFS-Cav-110:PwrFwd-"
ai15_ = "RFS-Cav-110:PwrRfl-"
ai16_ = "RFS-Load-120:PwrRfl-"
ai17_ = "RFS-Cav-110:Fld-"
ai18_ = "RFS-FIM-101:RF9-"
ai19_ = "RFS-FIM-101:RF10-"


di00_ = "RFS-SIM-110:HvEna-"
di01_ = "RFS-SIM-110:RfEna-"
di02_ = "RFS-Mod-110:PCcon-"
di03_ = "RFS-Mod-110:Ready-"
di04_ = "RFS-VacPS-110:I-SP-"
di05_ = "RFS-VacPS-120:I-SP-"
di06_ = "RFS-FIM-101:DI7-"
di07_ = "RFS-FIM-101:DI8-"
di08_ = "RFS-VacCav-110:Status-"
di09_ = "RFS-FIM-101:DI10-"
di10_ = "RFS-VacBody-110:Status-"
di11_ = "RFS-FIM-101:DI12-"
di12_ = "RFS-FIM-101:DI13-"
di13_ = "RFS-FIM-101:DI14-"
di14_ = "RFS-ADG-110:IlckStatus-"
di15_ = "RFS-ADG-110:PwrFail-"
di16_ = "RFS-FIM-101:DI17-"
di17_ = "RFS-FIM-101:DI18-"
di18_ = "RFS-FIM-101:DI19-"
di19_ = "RFS-FIM-101:DI20-"
di20_ = "RFS-LLRF-101:Status-"

rp00_ = "RFS-FIM-101:RP1"
rp01_ = "RFS-FIM-101:RP2"
cd00_ = "RFS-FIM-101:CD1"
cd01_ = "RFS-FIM-101:CD2"

########################################################################################################

# Adding the macros on the widget that will consume this script
widget.getPropertyValue("macros").add("IOC_", ioc_)
widget.getPropertyValue("macros").add("IOCSTATS_", iocstats_)


# Analog input macros
widget.getPropertyValue("macros").add("AI00", ai00_)
widget.getPropertyValue("macros").add("AI01", ai01_)
widget.getPropertyValue("macros").add("AI02", ai02_)
widget.getPropertyValue("macros").add("AI03", ai03_)
widget.getPropertyValue("macros").add("AI04", ai04_)
widget.getPropertyValue("macros").add("AI05", ai05_)
widget.getPropertyValue("macros").add("AI06", ai06_)
widget.getPropertyValue("macros").add("AI07", ai07_)
widget.getPropertyValue("macros").add("AI08", ai08_)
widget.getPropertyValue("macros").add("AI09", ai09_)
widget.getPropertyValue("macros").add("AI10", ai10_)
widget.getPropertyValue("macros").add("AI11", ai11_)
widget.getPropertyValue("macros").add("AI12", ai12_)
widget.getPropertyValue("macros").add("AI13", ai13_)
widget.getPropertyValue("macros").add("AI14", ai14_)
widget.getPropertyValue("macros").add("AI15", ai15_)
widget.getPropertyValue("macros").add("AI16", ai16_)
widget.getPropertyValue("macros").add("AI17", ai17_)
widget.getPropertyValue("macros").add("AI18", ai18_)
widget.getPropertyValue("macros").add("AI19", ai19_)

# Digital input macros
widget.getPropertyValue("macros").add("DI00", di00_)
widget.getPropertyValue("macros").add("DI01", di01_)
widget.getPropertyValue("macros").add("DI02", di02_)
widget.getPropertyValue("macros").add("DI03", di03_)
widget.getPropertyValue("macros").add("DI04", di04_)
widget.getPropertyValue("macros").add("DI05", di05_)
widget.getPropertyValue("macros").add("DI06", di06_)
widget.getPropertyValue("macros").add("DI07", di07_)
widget.getPropertyValue("macros").add("DI08", di08_)
widget.getPropertyValue("macros").add("DI09", di09_)
widget.getPropertyValue("macros").add("DI10", di10_)
widget.getPropertyValue("macros").add("DI11", di11_)
widget.getPropertyValue("macros").add("DI12", di12_)
widget.getPropertyValue("macros").add("DI13", di13_)
widget.getPropertyValue("macros").add("DI14", di14_)
widget.getPropertyValue("macros").add("DI15", di15_)
widget.getPropertyValue("macros").add("DI16", di16_)
widget.getPropertyValue("macros").add("DI17", di17_)
widget.getPropertyValue("macros").add("DI18", di18_)
widget.getPropertyValue("macros").add("DI19", di19_)
widget.getPropertyValue("macros").add("DI20", di20_)

# Reflected Power macros
widget.getPropertyValue("macros").add("RP0", rp00_)
widget.getPropertyValue("macros").add("RP1", rp01_)
widget.getPropertyValue("macros").add("CD0", cd00_)
widget.getPropertyValue("macros").add("CD1", cd01_)

